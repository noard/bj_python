import sys
I = sys.stdin.readline
c = []

for T in range(int(I())):
    cmd = I().strip().split()
    if cmd[0] == 'push':
        c.append(int(cmd[1]))
    if cmd[0] == 'pop':
        if len(c) > 0 :
            print(c.pop())
        else :
            print(-1)
    if cmd[0] == 'size':
        print(len(c))
    if cmd[0] == 'empty':
        print(1 if len(c)==0 else 0)
    if cmd[0] == 'top':
        if len(c) > 0:
            print(c[len(c)-1])
        else :
            print(-1)