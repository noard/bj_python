import sys

num = sys.stdin.readline().rstrip()
num = int(num)

for i in range(num):
  avg = 0.0
  cnt = 0
  a = list(map(int, sys.stdin.readline().rstrip().split()))
  avg = (sum(a, 0.0)-a[0])/a[0]
  for j in range(1, a[0]+1):
    if a[j] > avg:
      cnt += 1
  print("{0:.3f}%".format(cnt/a[0]*100, 3))