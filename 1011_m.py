import math
n=int(input())

for i in range(n):
    x, y = map(int,input().split())
    dist = y-x
    n = round(math.sqrt(dist))
    if dist >= n**2-n+1 and dist <= n**2:
        print(2*n-1)
    else:
        print(2*n)