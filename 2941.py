import sys

val = sys.stdin.readline().rstrip()
c2 = ["c=", "c-", "d-", "lj", "nj", "s=", "z="]
c3 = "dz="
cnt = 0
for i in c2:
  cnt += val.count(i)
cnt += val.count(c3)
 
print(len(val) - cnt)