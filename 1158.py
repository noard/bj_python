import sys
I = sys.stdin.readline

val, step = map(int, I().strip().split())

c = [i for i in range(1, val+1)]
out = '<'
ptr = 0

while len(c) > 0:
    ptr = (ptr + step - 1)%len(c)
    out += str(c.pop(ptr))+', '
print(out[0:-2], end='>')
