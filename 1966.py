import sys
from collections import deque
I = sys.stdin.readline

for i in range(int(I())):
    c = list(map(int, I().split()))
    d = list(map(int, I().split()))
    d[c[1]] = -d[c[1]]
    cnt=0
    while True:
        val = d[0]
        if abs(val) < max(d) or abs(val) < abs(min(d)):
            d.pop(0)
            d.append(val)
        elif val < 0:
            cnt += 1
            print(cnt)
            break
        else :
            d.pop(0)
            cnt += 1
