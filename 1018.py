import sys

row, col = sys.stdin.readline().rstrip().split()
row = int(row)
col = int(col)
min = 64
w_board = [["W", "B", "W", "B", "W", "B", "W", "B"] , ["B", "W", "B", "W", "B", "W", "B", "W"], ["W", "B", "W", "B", "W", "B", "W", "B"] , ["B", "W", "B", "W", "B", "W", "B", "W"], ["W", "B", "W", "B", "W", "B", "W", "B"] , ["B", "W", "B", "W", "B", "W", "B", "W"], ["W", "B", "W", "B", "W", "B", "W", "B"] , ["B", "W", "B", "W", "B", "W", "B", "W"]]
b_board = [["B", "W", "B", "W", "B", "W", "B", "W"], ["W", "B", "W", "B", "W", "B", "W", "B"] , ["B", "W", "B", "W", "B", "W", "B", "W"], ["W", "B", "W", "B", "W", "B", "W", "B"] , ["B", "W", "B", "W", "B", "W", "B", "W"], ["W", "B", "W", "B", "W", "B", "W", "B"] , ["B", "W", "B", "W", "B", "W", "B", "W"], ["W", "B", "W", "B", "W", "B", "W", "B"]]
r_val = [[0]*col for i in range(row)]
for i in range(row):
  r_val[i] = list(sys.stdin.readline().rstrip())

for i in range(row - 8 + 1):
  for j in range(col - 8 + 1):
    cnt = 0
    for m in range(8):
      for n in range(8):
        if ord(w_board[m][n]) + ord(r_val[i+m][j+n]) != ord("W") + ord("B"):
          cnt += 1;
    if cnt < min :
      min = cnt
    cnt = 0
    for m in range(8):
      for n in range(8):
        if ord(b_board[m][n]) + ord(r_val[i+m][j+n]) != ord("W") + ord("B"):
          cnt += 1;
    if cnt < min :
      min = cnt
print(min)