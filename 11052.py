import sys
I = sys.stdin.readline

n = int(I())
c = [0]*(n+1)
p = list(map(int, I().split()))

for i in range(1, n+1):
    for j in range(i, n+1):
        c[j] = max(c[j], c[j-i] + p[i-1])
print(c[n])