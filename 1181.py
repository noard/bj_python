import sys
I = sys.stdin.readline

s = [[''] for i in range(51)]
n = int(I())

for i in range(n):
    ins = str(I()).rstrip('\n')
    s[len(ins)].append(ins)

for i in range(51):
    s[i] = list(set(s[i]))
    s[i].sort()
    if(len(s[i])>1):
        for j in range(1,len(s[i])):
            print(s[i][j])