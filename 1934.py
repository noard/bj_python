import sys
input = sys.stdin.readline

def gcd(a, b):
    if a % b == 0:
        return b
    else:
        return gcd(b, a%b)

num = list(map(int, sys.stdin.readline().rstrip().split()))
for i in range(num[0]):
    a = list(map(int, sys.stdin.readline().rstrip().split()))    
    sorted(a)
    b = gcd(a[0], a[1])
    print(b*(a[0]//b)*(a[1]//b))