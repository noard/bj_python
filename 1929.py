import sys
I = sys.stdin.readline
s, e = list(map(int, I().rstrip().split()))
max_val = min(e+1,1000001)
b = [1 for i in range(1, max_val+1)]
b[0] = b[1] = 0

for i in range(4, max_val, 2):
    b[i] = 0
for i in range(3, max_val, 2):
    j = 2
    while i*j < max_val:
        b[i*j] = 0
        j += 1
for i in range(s, e+1):
    if b[i] == 1:
        print(i)