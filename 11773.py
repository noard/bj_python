import sys
I = sys.stdin.readline
c = []

for T in range(int(I())):
    inp = int(I())
    if inp == 0:
        c.pop()
    else:
        c.append(inp)
print(sum(c))
