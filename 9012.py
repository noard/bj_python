import sys
I = sys.stdin.readline

for T in range(int(I())):
    inp = I().strip()
    for i in range(len(inp)//2):
        inp = inp.replace('()',"")
    if len(inp) == 0:
        print('YES')
    else:
        print('NO')