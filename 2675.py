import sys

cnt = sys.stdin.readline().rstrip()
cnt = int(cnt)
for i in range(cnt):
  num, val = sys.stdin.readline().rstrip().split()
  num = int(num)
  val=list(val)
  out = ""
  for j in val:
    out = out + j*num

  print (out.strip())