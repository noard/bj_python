import sys
I = sys.stdin.readline

a = str(I().strip())
b = str(I().strip())

c = [[0]*(len(b)+1) for _ in range(len(a)+1)]

for i in range(len(b)+1):
    c[0][i] = i

for i in range(1, len(a)+1):
    c[i][0] = i
    for j in range(1, len(b)+1):
        if a[i-1] == b[j-1]:
            c[i][j] = c[i-1][j-1]
        else :
            c[i][j] = min(c[i-1][j-1], c[i-1][j], c[i][j-1])+1
    print(c)

print(c[-1][-1])
