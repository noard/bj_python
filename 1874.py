import sys
I = sys.stdin.readline
seq = []
val_stack = []
cur_val = 1
check = True
for T in range(int(I())):
    in_val = int(I())
    if in_val > cur_val:
        for i in range(cur_val, in_val+1) :
            seq.append('+')
            val_stack.append(i)
        seq.append('-')
        val_stack.pop()
        cur_val = in_val+1
        # print('in_val > cur_val', cur_val, seq, val_stack)
    elif in_val == cur_val:
        seq.append('+')
        seq.append('-')
        cur_val += 1
        # print('in_val = cur_val', cur_val, seq, val_stack)
    else:
        if in_val in val_stack:
            while True:
                seq.append('-')
                if val_stack.pop() == in_val:
                    break
                else:
                    seq.append('-')
        else:
            check = False
            break
        # print('in_val < cur_val', cur_val, seq, val_stack)
if check :
    print ('\n'.join(seq))
else :
    print ('NO')
