import sys

m, d = sys.stdin.readline().rstrip().split()
m = int(m)
d = int(d)
days=[31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
week = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
sum  = 0
if m > 1:
  for i in range(m-1):
    sum += days[i]
  sum += d - 1
else:
  sum += d - 1
sum %= 7
print(week[sum])