import sys
input = sys.stdin.readline
m,n = map(int,input().split())
board = [input().strip() for i in range(m)]
record = None
for row in range(m-7):
    for col in range(n-7):
        samp = ""
        for a in range(8):
            if a % 2 == 0:
                samp += board[row+a][col:col+8]
            else:
                samp += board[row + a][col:col + 8][::-1]
            print(samp)
        even = samp[1::2]
        odd = samp[::2]
        ans1 = even.count('W') + odd.count('B')
        ans2 = even.count('B') + odd.count('W')
        ans = min(ans1, ans2)

        if record == None:
            record = ans
        elif ans < record:
            record = ans
print(record)