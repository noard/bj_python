import math
x=str(input())
cnt_list = [0 for i in range(10)]

for i in x:
    cnt_list[int(i)] += 1

print(max(cnt_list[0], cnt_list[1], cnt_list[2], cnt_list[3], cnt_list[4], cnt_list[5], math.ceil((cnt_list[6]+cnt_list[9])/2), cnt_list[7], cnt_list[8]))
