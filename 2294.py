import sys
I = sys.stdin.readline

n, t = map(int, I().split())

c = [999999]*(t+2)
m = []
for i in range(n):
    m.append(int(I()))

m.sort()

for i in m:
    if i <= t :
        c[i] = 1
        for j in range(i+1, t+1):
            c[j] = min(c[j-i]+1, c[j])
if c[t] == 999999:
    print('-1')
else:
    print(c[t])