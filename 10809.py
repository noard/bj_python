import sys

val = sys.stdin.readline().rstrip()
out = ""
for i in range(ord('a'), ord('z')+1):
  out = out + " " + str(val.find(chr(i)))

print (out.strip())