import sys
I = sys.stdin.readline

a = {0:1, 1:2}
n, p, q = map(int, I().strip().split())


def dp(x):
    if x in a:
        return a[x]
    
    a[x] = dp(x//p) + dp(x//q)
    return a[x]

print(dp(n))