import sys
I = sys.stdin.readline
s = []
r_1 = [0]*300
r_2 = [0]*300

n = int(I())
for i in range(n):
    s.append(int(I()))

r_1[0] = s[0]
r_2[0] = 0
r_2[1] = s[0]+s[1]
r_1[1] = s[1]

for i in range(2, len(s)):
    r_1[i] = max(r_1[i-2], r_2[i-2])+s[i]
    r_2[i] = r_1[i-1]+s[i]
    # print(r_1, r_2)

print(max(r_1[n-1], r_2[n-1]))