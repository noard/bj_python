import sys

val = sys.stdin.readline().rstrip()
val = list(val)
out = ""
for i in val:
  if ord(i) >= ord("A") and ord(i) <= ord("Z"):
    out += i
print(out)