import sys
from collections import deque
I = sys.stdin.readline
q = deque()
for i in range(int(I())):
    s = I().strip().split()
    if s[0] == 'push_back':
        q.append(int(s[1]))
    elif s[0] == 'push_front':
        q.appendleft(int(s[1]))
    elif s[0] == 'pop_front':
        if len(q) > 0:
            print(q.popleft())
        else :
            print('-1')
    elif s[0] == 'pop_back':
        if len(q) > 0:
            print(q.pop())
        else :
            print('-1')
    elif s[0] == 'size':
        print(len(q))
    elif s[0] == 'empty':
        print('1' if len(q)==0 else '0')
    elif s[0] == 'front':
        if len(q) > 0:
            print(q[0])
        else :
            print('-1')
    elif s[0] == 'back':
        if len(q) > 0:
            print(q[-1])
        else:
            print('-1')