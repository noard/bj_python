import sys

num = sys.stdin.readline().rstrip()
num = int(num)

def move_tower(p_num, col_from, col_to, col_sub):
    if p_num == 1 :
        move_plate(col_from, col_to)
    else:
        move_tower(p_num-1, col_from, col_sub, col_to)
        move_plate(col_from, col_to)
        move_tower(p_num-1, col_sub, col_to, col_from)
       
    
def move_plate(col_from, col_to):
    print(str(col_from) + " " + str(col_to))

print(pow(2,num)-1)
move_tower(num, 1, 3, 2)
