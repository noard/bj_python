import sys
I = sys.stdin.readline

max_val = 100000

b = [0, 0] + [1]*(max_val-1)
c = [0]*(max_val+1)


for i in range(4, max_val+1, 2):
    b[i] = 0
for i in range(3, max_val+1, 2):
    if b[i] == 1:
        j = 2
        while i*j < max_val:
            b[i*j] = 0
            j += 1

def get_max_ali(num):
    if num != 1:
        for i in range(num, 1, -1):
            if(b[i] == 1):
                if(num%i == 0):
                    c[i] += 1
                    get_max_ali(num//i)
                    break

n = int(I())
for i in range(n):
    val = int(I())
    c = [0]*(val+1)
    get_max_ali(val)
    for j in range(2, val+1):
        if c[j] > 0:
            print(j, c[j])
