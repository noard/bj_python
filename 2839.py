import sys

a = int(sys.stdin.readline())
cnt = 0
if a % 5 == 0:
  print(int(a/5))
else:
  for i in range(1, int(a/3) + 1):
    if (a-(3*i))%5 == 0:
      cnt = i
      break;
  if cnt == 0:
    print("-1")
  else:
    print(cnt + int((a-(3*cnt))/5))
    