import sys
I = sys.stdin.readline

while True:
    s = I().rstrip()
    if s == '.':
        break;
    c = []
    t = True
    for i in s:
        if i == '(':
            c.append('(')
        elif i == ')':
            if(c.pop() != '(' or len(c) == 0):
                t = False
                break
        elif i == '[':
            c.append('[')
        elif i == ']':
            if(c.pop() != '[' or len(c) == 0):
                t = False
                break

    if (t == False) or (len(c) > 0):
        print('no')
    else :
        print('yes')
