n = int(input())

t = '  *  \n * * \n*****\n'

def m_star(n, k):
    r = []
    k = k.split('\n')
    n //= 2
    for i in range(n):
        r.append(' '*(n) + k[i] + ' '*(n))
    for i in range(n) :
        r.append(k[i] + ' ' + k[i])
    return '\n'.join(r)

if n//3 == 1:
    print(t)
else:
    for i in range (1, 11):
        t = m_star(3*(2**i), t)
        if (3*(2**i)) == n:
            break
    print(t)
