import sys
I = sys.stdin.readline

for _ in range(int(I())):
    n = int(I())
    a1 = list(map(int, I().split()))
    a2 = list(map(int, I().split()))
    a = b = c = 0
    for i in range(n):
        a, b, c = a1[i] + max(b,c), a2[i]+max(a,c), max(a,b)
    print(max(a,b))