import sys
from collections import deque
I = sys.stdin.readline

for _ in range(int(I())):
    check = True
    cmd = I().strip()
    cnt = int(I())
    out = '['
    if cnt == 0:
        I()
        n = []
    else:
        n = deque(list(map(int, I().strip()[1:-1].split(','))))
    reverse = False
    
    for i in cmd:
        # print(n)
        if i == 'R':
            reverse = not reverse
        elif i == 'D':
            if len(n) > 0:
                if reverse :
                    n.pop()
                else:
                    n.popleft()
                # print('pop',len(n))
            else:
                check = False
                # print('fail')
                break
    if(check):
        if reverse :
            n.reverse()
            out += ','.join(map(str,n))+']'
            print(out)
        else:
            out += ','.join(map(str,n))+']'
            print(out)
    else:
        print('error')
