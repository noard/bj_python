import sys

val = sys.stdin.readline().rstrip()
val = int(val)
cnt = 0
if val == 1000:
  val = 999;
for i in range(1, val+1):
  if i < 100:
    cnt += 1
  else:
      t_val = list(str(i))
      if int(t_val[0]) - int(t_val[1]) == int(t_val[1]) - int(t_val[2]):
        cnt +=1
print(cnt)