import sys
input = sys.stdin.readline().rstrip()
input = int(input)
matrix = [['*' for col in range(input)] for row in range(input)]

def draw(col, row, n):
    m_n = int(n/3)
    if m_n == 1 :
        mk_box(col, row, m_n)
    else :
        mk_box(col, row, m_n)
        for m_col in range (3):
            for m_row in range (3):
                draw(m_n*m_col+col, m_n*m_row+row, m_n)

def mk_box(col, row, n):
    global matrix
    for m_col in range (col+n, col+(2*n)):
            for m_row in range (row+n, row+(2*n)):
                matrix[m_col][m_row] = ' '

draw(0, 0, input)
for col in range(input):
    for row in range(input):
        print(matrix[col][row], end='')
    print('')

