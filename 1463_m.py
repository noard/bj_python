import sys
I = sys.stdin.readline

n = int(I())

a = [0,0,1,1,2]
if n > 4:
    for i in range(5,n+1):
        val = a[i-1]
        if i%3 == 0:
            val = min(val, a[i//3])
        if i%2 == 0:
            val = min(val, a[i//2])
        a.append(val+1)

print(a[n])