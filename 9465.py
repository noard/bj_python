import sys
I = sys.stdin.readline
for _ in range(int(I())):
    c = []
    t = [0,0]
    n = int(I())
    c.append(list(map(int, I().split())))
    c.append(list(map(int, I().split())))
    t = [[0]*n, [0]*n]
    for i in range(n):
        if i == 0:
            t[0][i] = c[0][i]
            t[1][i] = c[1][i]
        elif i == 1:
            t[0][i] = t[1][0] + c[0][i]
            t[1][i] = t[0][0] + c[1][i]
        else:
            t[0][i] = c[0][i]+max(t[1][i-1], t[0][i-2], t[1][i-2])
            t[1][i] = c[1][i]+max(t[0][i-1], t[0][i-2], t[1][i-2])
        # print(t)
    print(max(t[0][n-1],t[1][n-1]))