import sys
I = sys.stdin.readline
seq = ''
val_stack = []
cur_val = 1
check = True
for T in range(int(I())):
    in_val = int(I())
    while in_val >= cur_val:
        seq += '+\n'
        val_stack.append(cur_val)
        cur_val += 1
    if val_stack[-1] == in_val:
        seq += '-\n'
        val_stack.pop()
    else:
        check = False   

if check:
    print(seq, end = '')
else:
    print('NO')