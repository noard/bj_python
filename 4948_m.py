import sys
I = sys.stdin.readline

max_val = 123456*2

b = [0, 0] + [1]*(max_val-2)

for i in range(4, max_val, 2):
    b[i] = 0
for i in range(3, max_val, 2):
    if b[i] == 1:
        j = 2
        while i*j < max_val:
            b[i*j] = 0
            j += 1

while True:
    a = int(I())
    if a == 0:
        break
    else:
        print(sum(b[a+1:a*2+1]))