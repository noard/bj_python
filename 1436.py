import sys

target_cnt = sys.stdin.readline().rstrip()
target_cnt = int(target_cnt)

cur_val = 666
cnt = 1;

while(1):
  if(cnt == target_cnt):
    print(cur_val)
    break
  cur_val += 1
  if '666' in str(cur_val):
    cnt += 1