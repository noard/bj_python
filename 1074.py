import sys
input = sys.stdin.readline
n, r, c = list(map(int, sys.stdin.readline().rstrip().split()))
cnt = 0

def search_num (n, r, c) :
    global cnt
    block = 2**(n-1)
    if (n == 1):
        cnt += (r*2) + c
    else :
        if (r < block):
            if (c < block): # 1사분면
                search_num(n-1, r, c)
            else : # 2사분면
                cnt += block**2
                search_num(n-1, r, c-block )
        else :
            if (c < block): # 3사분면
                cnt += (block**2)*2
                search_num(n-1, r-block, c)
            else : # 4사분면
                cnt += (block**2)*3
                search_num(n-1, r-block, c-block)

search_num(n, r, c)
print(cnt)