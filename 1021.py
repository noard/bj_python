import sys
from collections import deque
I = sys.stdin.readline

n = list(map(int, I().split()))

c = deque([i for i in range(1, n[0]+1)])

m = list(map(int, I().split()))
cnt = 0
for i in m:
    if c[0] == i:
        c.popleft()
    else:
        # print('index:', c.index(i), 'len(c)-c.count:', len(c)-c.index(i))
        t_val = c.index(i)
        if t_val <= len(c)-t_val:
            c.rotate(-t_val)
            cnt += t_val
            # print('if',c, cnt)
            c.popleft()
        else:
            c.rotate(len(c)-t_val)
            cnt += len(c)-t_val
            # print('else',c,cnt)
            c.popleft()

print(cnt)