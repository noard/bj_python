import sys
I = sys.stdin.readline

c = [1]+[0]*10000
m = []

n, t = map(int, I().split())

for i in range(n):
    
    m.append(int(I()))

m.sort()

for i in m:
    for j in range(i, t+1):
        c[j] += c[j-i]
    # print(c[:11])

print(c[t])