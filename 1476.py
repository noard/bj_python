import sys

e, s, m = sys.stdin.readline().rstrip().split()
e = int(e)
s = int(s)
m = int(m)
cnt = 0
year = 0

while True:
  year = s + (28 * cnt)
  if (year - e) % 15 == 0 and (year-m) % 19 == 0:
    print(year)
    break
  else:
    cnt += 1