import sys
I = sys.stdin.readline

c = [0,1,3]

n = int(I())

if n > 2:
    for i in range(n-2):
        c.append((c[-1]%10007+(c[-2]*2)%10007)%10007)

print(c[n])
