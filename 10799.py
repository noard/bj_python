import sys
from time import sleep
I = sys.stdin.readline

s = I().strip()
s = s.replace('()','x')
cnt = 1
total = 0
while (s.count('(') != 0):
    target = '('+'x'*cnt+')'
    while (s.count(target) != 0):
        total = total + s.count(target)*(cnt+1)
        s = s.replace(target, 'x'*cnt)
    cnt += 1

print(total)
