import sys
I = sys.stdin.readline

c = [0,1]
for i in range(2, int(I())+1):
    c.append(c[i-2] + c[i-1])

print(c[-1])