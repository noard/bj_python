import sys
from time import sleep
sys.setrecursionlimit(1000000000)
I = sys.stdin.readline

n = int(I())

a = {1:0, 2:1, 3:1, 4:2, 5:3}

def dp(x, lv):

    if a.get(x) != None:
        return a[x]
    
    print('lv', lv, 'x', x)
    if x%3 == 0 and x%2 == 0:
        a[x] = min(dp(x-1, lv+1), dp(x//2, lv+1), dp(x//3, lv+1))+1
    elif x%3 == 0:
        a[x] = min(dp(x-1, lv+1), dp(x//3, lv+1))+1
    elif x%2 == 0:
        a[x] = min(dp(x-1, lv+1), dp(x//3, lv+1))+1
    else:
        a[x] = dp(x-1, lv+1)+1
    sleep(0.05)
    return a[x]
    
print(dp(n, 0))
# print(a)