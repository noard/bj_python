import sys
from collections import deque
I = sys.stdin.readline

c = deque([i for i in range(1, int(I())+1)])

while len(c) > 1:
    c.popleft()
    c.rotate(-1)
    # print(c)
print(c[0])
