import sys
I = sys.stdin.readline

max_val = 10000

b = [0, 0] + [1]*(max_val-2)

for i in range(4, max_val, 2):
    b[i] = 0
for i in range(3, max_val, 2):
    if b[i] == 1:
        j = 2
        while i*j < max_val:
            b[i*j] = 0
            j += 1

n = int(I())

for i in range(n):
    val = int(I())
    for j in range(val//2, 1, -1):
        if b[j] == 1 and b[val - j]:
            print(j, val-j)
            break