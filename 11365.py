import sys

while True:
  val = sys.stdin.readline().rstrip()
  if val == "END":
    break
  else:
    print(val[::-1])