import sys

I = sys.stdin.readline
max_val = 1000001
b = [1 for i in range(1, max_val+1)]
b[0] = b[1] = 0

b[4::2] = [0]*len(b[4::2])
for i in range(3, int(max_val**0.5 + 1.5), 2):
    b[i*2::i] = [0]*len(b[i*2::i])

while 1 :
    n = list(map(int, I().rstrip().split()))

    m_flag = False
    if n[0] != 0:
        for k in range(3, (n[0]//2)+1):
            if b[k] == 1 and b[n[0]-k] == 1:
                print(n[0], "=", k, "+", n[0]-k)
                m_flag = True
                break;
        if m_flag == False:
            print("Goldbach's conjecture is wrong.")
    else:
        break;