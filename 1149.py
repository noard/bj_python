import sys
I = sys.stdin.readline

n = int(I())

for i in range(n):
    c = list(map(int, I().split()))
    if i == 0:
        r = c
    else :
        tmp = list(r)
        r[0] = c[0] + min(tmp[1], tmp[2])
        r[1] = c[1] + min(tmp[0], tmp[2])
        r[2] = c[2] + min(tmp[0], tmp[1])

print (min(r))
