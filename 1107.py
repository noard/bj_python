import sys
input = sys.stdin.readline
base = [True for i in range(10)]
target_b = []
target_s = []
a = input().rstrip()
b = list(a)
a = int(a)
num_f = input().rstrip()
f = list(map(int, input().rstrip().split()))
min_c = 500000
for i in f:
  base[i] = False

for i in range(a, 999999):
  t = list(str(i))
  cnt_f = True
  for j in t:
    if base[int(j)] == False:
      cnt_f = False
      break
  if cnt_f == True:
    if min_c > len(t) + abs(int(''.join(t)) - a):
      min_c = len(t) + abs(int(''.join(t)) - a)
    break
for i in range(a, -1, -1):
  t = list(str(i))
  cnt_f = True
  for j in t:
    if base[int(j)] == False:
      cnt_f = False
      break
  if cnt_f == True:
    if min_c > len(t) + abs(int(''.join(t)) - a):
      min_c = len(t) + abs(int(''.join(t)) - a)
    break

if abs(100 - a) < min_c:
  min_c = abs(100-a)
    
print(min_c)