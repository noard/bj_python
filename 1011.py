import math
n=int(input())

for i in range(n):
    x, y = map(int,input().split())
    dist = y-x
    cnt = 1
    while True:
        if cnt%2 == 1:
            if ((cnt//2)+1) * ((cnt//2)+1) == dist:
                print(cnt)
                break
            elif ((cnt//2)+1) * ((cnt//2)+1) > dist:
                print(cnt + (dist-(((cnt-1)//2)+1)*((cnt-1)//2))//(cnt-1))
                break
            else:
                cnt += 1
        else:
            if ((cnt//2)+1) * (cnt//2) == dist:
                print(cnt)
                break
            elif ((cnt//2)+1) * (cnt//2) > dist:
                print(cnt + (dist-((((cnt-1)//2)+1)*(((cnt-1)//2)+1)))//(cnt-1))
                break
            else:
                cnt += 1