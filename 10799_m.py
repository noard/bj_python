import sys
I = sys.stdin.readline

s = I().strip()
s = s.replace('()','x')
c = []
cnt = 0;
for i in s:
    if i == '(' :
        c.append('(')
    elif i == 'x':
        cnt += len(c)
    else :
        c.pop()
        cnt += 1

print(cnt)        