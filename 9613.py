import sys
input = sys.stdin.readline

def gcd(a, b):
    if a % b == 0:
        return b
    else:
        return gcd(b, a%b)

n = list(map(int, input().rstrip().split()))
for i in range(n[0]):
    a = list(map(int, input().rstrip().split()))
    sum = 0
    for j in range(1, a[0]):
        for k in range(j+1, a[0]+1):
           sum += gcd(a[j], a[k])
    print(sum)