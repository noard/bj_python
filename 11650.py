import sys
I = sys.stdin.readline
max = 200001
s = [[-100001] for i in range(max)]
n = int(I())

for i in range(n):
    ins = list(map(int, I().rstrip().split()))
    s[ins[0]+100000].append(ins[1])

for i in range(max):
    s[i].sort()
    if(len(s[i])>1):
        for j in range(1,len(s[i])):
            print(i-100000, s[i][j])