import sys

num = sys.stdin.readline().rstrip()
num = int(num)
a = list(map(int, sys.stdin.readline().rstrip().split()))
a.sort(reverse=True)
sum = 100.0

for i in range(1, num):
  sum += a[i]/a[0] * 100

print(sum/num)