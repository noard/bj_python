import sys
import math
I = sys.stdin.readline

for i in range(int(I())):
    n = int(I())
    i = 2
    while i*i <= n:
        cnt = 0
        if n%i == 0:
            while (n%i == 0):
                n = n//i
                cnt += 1
            print(i, cnt)
            i += 1
        else:
            i += 1
    if n > 1:
        print(n, 1)