import sys

a, b= sys.stdin.readline().rstrip().split()
b = int(b)

c = sys.stdin.readline().rstrip().split()

out = ""
for i in c:
  if int(i) < b:
    out = out + " " + i

print(out.strip())